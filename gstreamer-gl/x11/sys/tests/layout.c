// Generated by gir (https://github.com/gtk-rs/gir @ e43ea0b348f0)
// from gir-files (https://github.com/gtk-rs/gir-files @ 01c4ec663b3f)
// from gst-gir-files (https://gitlab.freedesktop.org/gstreamer/gir-files-rs.git @ 40cea11af0f3)
// DO NOT EDIT

#include "manual.h"
#include <stdalign.h>
#include <stdio.h>

int main() {
    printf("%s;%zu;%zu\n", "GstGLDisplayX11", sizeof(GstGLDisplayX11), alignof(GstGLDisplayX11));
    printf("%s;%zu;%zu\n", "GstGLDisplayX11Class", sizeof(GstGLDisplayX11Class), alignof(GstGLDisplayX11Class));
    return 0;
}
